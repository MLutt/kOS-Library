// Ensure bootfile to only run on launch
if exists("flags/booted") {
    runPath("ship/active.ks").
} else {
    if not exists("flags") {
        createDir("flags").
    }
    create("flags/booted").
    print("Getting scripts for vessel '" + shipName + "'...").

    // Copying library
    copyPath("0:/lib/GLOBAL", path() + "/lib").

    // Copying ship-specific scripts

    if not exists("0:/vessels/" + shipName) {
        createDir("0:/vessels/" + shipName).
        copyPath("0:/lib/defaultBoot.ks", "0:/vessels/" + shipName + "/boot.ks").
        copyPath("0:/lib/defaultActive.ks", "0:/vessels/" + shipName + "/active.ks").
    }
    copyPath("0:/vessels/" + shipName, path() + "/ship").

    runPath("ship/boot.ks").
}
