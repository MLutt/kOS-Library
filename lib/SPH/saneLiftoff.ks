DECLARE PARAMETER climbingPitch.
DECLARE PARAMETER targetAltitude.

toggle brakes.
print("waiting for brakes to be disabled (let the jiggle stop...)").
wait until not brakes.
print("starting runway procedure").
lock steering to heading(90, (90 - VANG(ship:up:forevector,ship:facing:forevector)), 0).
lock throttle to 1.
stage.
wait until activeShip:status = "FLYING".
print("starting midair procedure").
steeringManager:resettodefault().
lock steering to heading(90, climbingPitch, 0).
//set throttle to 1.
toggle gear.
wait until altitude >= targetAltitude.
//toggle sas.
//lock throttle to 1.
//print("completed liftoff sequence, switching to interactive SAS mode").
