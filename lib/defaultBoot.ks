print("Default boot file for unknown ship. This script will only run during the ship's launch sequence, use 'active.ks' for execution on physics init.").
print("You can find and edit the ship's scripts here:").
print("'{KSP-Installation}/Ships/Script/vessels/" + shipName + "'").
